﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour 
{
    /* Constant string that defines the name of the player object in the unity scene */
    private const string PLAYER_NAME = "Player";
    /* Constant float that defines the length of time that the enemy will continue to hunt even after losing sight of the player. */
    private const float DEFAULT_TIME_TO_HUNT = 3f;
    /* Constant float that defines the starting/max health of an enemy */
    private const float MAX_HEALTH = 50f;
    /* Constant float that defines the walking speed of an enemy */
    private const float WALKING_SPEED = 3f;
    /* Constant float that defines the rotation speed of an enemy */
    private const float ROTATION_SPEED = 30f;



    /* GameObject that holds the player object 
     * Set in Start()
     * 0 setters
     * 0 getters */
    private GameObject player;

    /* Rigidbody object that holds the current enemy rigidbody instance 
     * Set in Start()
     * 0 setters
     * 0 getters */
    private Rigidbody rBody;



    /* Float that holds the current health of the enemy 
     * Should not go above max health
     * 3 setters --> SetHealth(float)
     *           --> Heal(float)
     *           --> Hurt(float)
     * 1 getter --> GetHealth() */
    private float health;
    /* Float that holds the current speed of the enemy 
     * 1 setter --> SetCurrentSpeed(float)
     * 1 getter --> GetCurrentSpeed() */
    private float currentSpeed;
    /* Float that holds the rotation speed of the enemy 
     * 1 setter --> SetRotationSpeed(float)
     * 1 getter --> GetRotationSpeed() */
    private float rotationSpeed;

    /* Boolean to tell whether the enemy is alive or dead 
     * True = alive
     * False = dead
     * 1 setter --> SetAlive(bool)
     * 1 getter --> IsAlive() */
    private bool alive;



    private RaycastHit hit;

    public int maxViewAngle;
    private int angle = 0;
    private bool addAngle = true;
    public int viewDistance = 20;
    public int maxAnglePerRay = 30;
    private int numRays;

    private float timeToHunt = 0;

    private GameObject nextCell;
    private GameObject currentCell;
    private GameObject previousCell;

	void Start () 
    {
        player = GameObject.Find(PLAYER_NAME);
        rBody = GetComponent<Rigidbody>();
        SetHealth(MAX_HEALTH);
        SetCurrentSpeed(WALKING_SPEED);
        SetRotationSpeed(ROTATION_SPEED);
        SetAlive(true);


        numRays = maxViewAngle / maxAnglePerRay;
        nextCell = currentCell = previousCell = null;
	}
    void FixedUpdate () 
    {
        Transform preyTransform;
        preyTransform = FindPlayer();
        if (preyTransform == null && timeToHunt <= 0) Roam();
        else Hunt(preyTransform);
        checkCellPosition();
    }



    /* Sets the health of the enemy 
     * If the health is being set to above the max health, the health will be set to max health
     * If the health gets below 0 then the enemy is set to be dead */
    public void SetHealth(float health)
    {
        this.health = health;
        if (GetHealth() > MAX_HEALTH) this.health = MAX_HEALTH;
        else if (GetHealth() <= 0) SetAlive(false);
    }
    /* Adds health to the enemy by a specified amount */
    public void Heal(float health)
    {
        SetHealth(GetHealth() + health);
    }
    /* Subtracts health from the enemy by a specified amount */
    public void Hurt(float health)
    {
        SetHealth(GetHealth() - health);
    }
    /* Sets the current speed of the enemy */
    public void SetCurrentSpeed(float currentSpeed)
    {
        this.currentSpeed = currentSpeed;
    }
    /* Sets the rotation speed of the enemy */
    public void SetRotationSpeed(float rotationSpeed)
    {
        this.rotationSpeed = rotationSpeed;
    }
    /* Sets whether the enemy is alive or not */
    public void SetAlive(bool alive)
    {
        this.alive = alive;
        if (!alive) Kill();
    }



    /* Returns a float that holds the enemy's health */
    public float GetHealth()
    {
        return health;
    }
    /* Returns a float that holds the enemy's current speed */
    public float GetCurrentSpeed()
    {
        return currentSpeed;
    }
    /* Returns a float that holds the enemy's rotation speed */
    public float GetRotationSpeed()
    {
        return rotationSpeed;
    }
    /* Returns a bool that states whether or not the enemy is alive */
    public bool IsAlive()
    {
        return alive;
    }



    private void Kill()
    {
        gameObject.SetActive(false);
    }




















    private Transform FindPlayer() 
    {
        if (angle >= maxAnglePerRay) addAngle = false;
        else if (angle <= 0) addAngle = true;
        if (addAngle) angle++;
        else angle--;

        Quaternion rotation;
        Vector3 rayDirection = transform.forward;

        for (int index = 0; index < numRays; index++) 
        {
            rotation = Quaternion.AngleAxis(maxViewAngle/-2 + (index*maxAnglePerRay) + angle, Vector3.up);
            rayDirection = rotation * transform.forward;

            Debug.DrawRay(transform.position, rayDirection * hit.distance, Color.blue);
            if(Physics.Raycast(transform.position, rayDirection, out hit, 20)) 
            {
                if (hit.transform.gameObject.name == "Player") 
                {
                    timeToHunt = DEFAULT_TIME_TO_HUNT;
                    return hit.transform;
                }
            }
        }
        return null;
    }
    private void Roam() 
    {
        if (currentCell == null) return;
        if(nextCell == null || nextCell == currentCell)
        {
            //print(nextCell);
            List<GameObject> availableCellsList = new List<GameObject>();
            GameObject northWall = currentCell.GetComponent<CellObject>().GetWall(CellObject.Direction.North);
            GameObject southWall = currentCell.GetComponent<CellObject>().GetWall(CellObject.Direction.South);
            GameObject eastWall = currentCell.GetComponent<CellObject>().GetWall(CellObject.Direction.East);
            GameObject westWall = currentCell.GetComponent<CellObject>().GetWall(CellObject.Direction.West);

            GameObject northCell = currentCell.GetComponent<CellObject>().GetCell(CellObject.Direction.North);
            GameObject southCell = currentCell.GetComponent<CellObject>().GetCell(CellObject.Direction.South);
            GameObject eastCell = currentCell.GetComponent<CellObject>().GetCell(CellObject.Direction.East);
            GameObject westCell = currentCell.GetComponent<CellObject>().GetCell(CellObject.Direction.West);
            switch (currentCell.GetComponent<CellObject>().GetCellType())
            {
                case CellObject.Type.Deadend: 
                    if(previousCell != null) nextCell = previousCell;
                    else 
                    {
                        if (northWall == null) nextCell = northCell;
                        else if (southWall == null) nextCell = southCell;
                        else if (eastWall == null) nextCell = eastCell;
                        else if (westWall == null) nextCell = westCell;
                    }
                    break;
                case CellObject.Type.Walkway:
                case CellObject.Type.Corner:
                    if (northWall == null && northCell != previousCell) nextCell = northCell;
                    else if (southWall == null && southCell != previousCell) nextCell = southCell;
                    else if (eastWall == null && eastCell != previousCell) nextCell = eastCell;
                    else if (westWall == null && westCell != previousCell) nextCell = westCell;
                    break;
                case CellObject.Type.Fork:
                case CellObject.Type.Open:
                    if (northWall == null && northCell != previousCell) availableCellsList.Add(northCell);
                    if (southWall == null && southCell != previousCell) availableCellsList.Add(southCell);
                    if (eastWall == null && eastCell != previousCell) availableCellsList.Add(eastCell);
                    if (westWall == null && westCell != previousCell) availableCellsList.Add(westCell);
                    nextCell = availableCellsList[Random.Range(0, availableCellsList.Count)];
                    break;
            }
            if (nextCell != null) transform.LookAt(nextCell.transform);
        }
        else 
        {
            rBody.MovePosition(rBody.position + transform.forward * GetCurrentSpeed() * Time.fixedDeltaTime);
        }
    }
    private void Hunt(Transform preyTransform) 
    {
        transform.LookAt(preyTransform);
        rBody.MovePosition(rBody.position + transform.forward * GetCurrentSpeed() * Time.fixedDeltaTime);
        timeToHunt -= Time.deltaTime;
        nextCell = currentCell; // Only used to go back into the loop when the enemy lost the player after hunting
    }
    private void checkCellPosition() 
    {
        if (Physics.Raycast(transform.position, Vector3.down, out hit))
        {
            if (hit.transform.gameObject.name.Contains("Cell"))
            {
                if (!hit.transform.GetComponent<CellObject>().Equals(currentCell)) 
                {
                    previousCell = currentCell;
                    currentCell = hit.transform.gameObject;
                }
            }
        }
    }
}