﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameButtonManager : MonoBehaviour {

    public GameObject pauseButton;
    public GameObject resumeButton;

    public Text ammoText;
    public Text hitMarkerText;

    private float hitMarkerTimer = 10000;

    void Start() {
        SetButtonActive(pauseButton);

        SetButtonInactive(resumeButton);

        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    private void Update()
    {
        UpdateAmmoText();
        hitMarkerTimer += Time.deltaTime * 1000;
        if (hitMarkerTimer <= 100) hitMarkerText.text = "X";
        else hitMarkerText.text = "";
    }

    public void PauseButton() {
        SetButtonInactive(pauseButton);

        SetButtonActive(resumeButton);

        Time.timeScale = 0;
    }
    public void ResumeButton() {
        SetButtonInactive(resumeButton);

        SetButtonActive(pauseButton);

        Time.timeScale = 1;
    }

    private void SetButtonActive(GameObject button) {button.SetActive(true);}
    private void SetButtonInactive(GameObject button) {button.SetActive(false);}



    private void UpdateAmmoText()
    {
        ammoText.text = PlayerObject.player.GetCurrentWeapon().GetBulletsInMagazine() + " / " + PlayerObject.player.GetCurrentWeapon().GetTotalNumBullets();
    }

    public void Hit()
    {
        hitMarkerTimer = 0;
    }
}