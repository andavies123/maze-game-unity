﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunObject : MonoBehaviour 
{
    /// <summary>
    /// The type of gun
    /// </summary>
    public enum Type { Pistol }
    /// <summary>
    /// How many bullets come out when the player shoots
    /// </summary>
    public enum FireType { Single, Auto }



    /// <summary>
    /// The max bullets each magazine can carry.
    /// </summary>
    private int maxBulletsPerMagazine;
    /// <summary>
    /// The amount of bullets in the current magazine
    /// </summary>
    private int bulletsInMagazine;
    /* Int that holds the total amount of bullets the player has to offer
     * Once this runs down to 0 then the player can no longer shoot the gun
     * 1 setter --> SetTotalNumBullets(int)
     * 1 getter --> GetTotalNumBullets()
     */
    private int totalNumBullets;

    /* Float that corresponds to the damage per shot
     * In other terms, for every bullet that hits, this is the damage it will inflict
     * 1 setter --> SetDamagePerShot(float)
     * 1 getter --> GetDamagePerShot() */
    private float damagePerShot;
    /* Float that corresponds to the time inbetween each shot
     * Time in milliseconds
     * 1 setter --> SetTimeBetweenShots(float)
     * 1 getter --> GetTimeBetweenShots() */
    private float timeBetweenShots;
    /// <summary>
    /// The time it takes to reload.
    /// </summary>
    private float timeToReload;
    /// <summary>
    /// The reload timer.
    /// </summary>
    private float reloadTimer;
    /// <summary>
    /// The shoot timer.
    /// </summary>
    private float shootTimer;

    /// <summary>
    /// Determines whether or not the gun is currently reloading.
    /// </summary>
    private bool isReloading;

    /// <summary>
    /// The type of the gun.
    /// </summary>
    private Type gunType;
    /// <summary>
    /// How the gun can fire bullets
    /// </summary>
    private FireType fireType;



    /// <summary>
    /// Start this instance.
    /// </summary>
    protected virtual void Start()
    { enabled = true; }
    /// <summary>
    /// Update this instance.
    /// </summary>
    protected virtual void Update()
    {
        UpdateTimers(Time.deltaTime);
        if(IsReloading() && GetReloadTimer() > GetTimeToReload())
        {
            if(GetMaxBulletsPerMagazine() - GetBulletsInMagazine() > GetTotalNumBullets())
            {
                SetBulletsInMagazine(GetBulletsInMagazine() + GetTotalNumBullets());
                SetTotalNumBullets(0);
            }
            else 
            {
                SetTotalNumBullets(GetTotalNumBullets() - (GetMaxBulletsPerMagazine() - GetBulletsInMagazine()));
                SetBulletsInMagazine(GetMaxBulletsPerMagazine());
            }
            SetIsReloading(false);
        }
    }



    /// <summary>
    /// Shoots one bullet from the gun
    /// </summary>
    /// <returns>The gameobject that was hit</returns>
    /// <param name="bulletSource">Where the bullet will come from</param>
    public GameObject Shoot(Transform bulletSource)
    {
        if(CanShoot())
        {
            SetBulletsInMagazine(GetBulletsInMagazine() - 1); // Takes a bullet out of the magazine
            ResetShootTimer();

            RaycastHit hit; // Holds the hit object that the ray hits
            if (Physics.Raycast(bulletSource.position, bulletSource.forward, out hit)) // Shoots a raycast forward from the camera
            {
                Debug.DrawRay(bulletSource.position, bulletSource.forward * hit.distance, Color.red); // Only for Debug purposes to see the shot
                return hit.transform.gameObject;
            }
        }
        return null;
    }
    /// <summary>
    /// Reloads the gun
    /// </summary>
    public void Reload()
    {
        if(CanReload() && !IsReloading())
        {
            ResetReloadTimer();
            SetIsReloading(true);
        }
    }



    /// <summary>
    /// Resets the shoot timer.
    /// </summary>
    private void ResetShootTimer()
    {
        this.shootTimer = 0;
    }
    /// <summary>
    /// Resets the reload timer.
    /// </summary>
    private void ResetReloadTimer()
    {
        this.reloadTimer = 0;
    }
    /// <summary>
    /// Updates the timers.
    /// </summary>
    /// <param name="deltaTime">Delta time.</param>
    private void UpdateTimers(float deltaTime)
    {
        reloadTimer += deltaTime * 1000;
        shootTimer += deltaTime * 1000;
    }

    /// <summary>
    /// Returns whether the weapon is ready to shoot or not
    /// </summary>
    /// <returns><c>true</c>, if the weapon is ready to shoot, <c>false</c> otherwise.</returns>
    private bool CanShoot()
    {
        return
            GetBulletsInMagazine() > 0 && // If there are still bullets in the magazine AND
            GetShootTimer() > GetTimeBetweenShots() && // If the shoot timer passed the amount it takes between shots AND
            !IsReloading(); // If the player is not reloading
    }
    /// <summary>
    /// Returns whether the weapon is ready to reload or not
    /// </summary>
    /// <returns><c>true</c>, if the weapon is ready to reload, <c>false</c> otherwise.</returns>
    private bool CanReload()
    {
        return
            GetBulletsInMagazine() < GetMaxBulletsPerMagazine() && // If the gun has to have at least shot one bullet AND
            GetTotalNumBullets() > 0 && // If there are enough bullets to load into the magazine
            !IsReloading(); // If the gun is not currently reloading
    }

    /// <summary>
    /// Sets the number of bullets per magazine.
    /// </summary>
    /// <param name="maxBulletsPerMagazine">Bullets per magazine.</param>
    public void SetMaxBulletsPerMagazine(int maxBulletsPerMagazine)
    {
        this.maxBulletsPerMagazine = maxBulletsPerMagazine;
    }
    /// <summary>
    /// Sets the number of bullets in the current magazine.
    /// </summary>
    /// <param name="bulletsInMagazine">Bullets in the current magazine.</param>
    public void SetBulletsInMagazine(int bulletsInMagazine)
    {
        this.bulletsInMagazine = bulletsInMagazine;
    }
    /// <summary>
    /// Sets the total number bullets the player has.
    /// </summary>
    /// <param name="totalNumBullets">Total number bullets.</param>
    public void SetTotalNumBullets(int totalNumBullets)
    {
        this.totalNumBullets = totalNumBullets;
    }

    /// <summary>
    /// Sets the damage per shot.
    /// </summary>
    /// <param name="damagePerShot">Damage per shot.</param>
    public void SetDamagePerShot(float damagePerShot)
    {
        this.damagePerShot = damagePerShot;
    }
    /// <summary>
    /// Sets the time between shots.
    /// </summary>
    /// <param name="timeBetweenShots">Time between shots.</param>
    public void SetTimeBetweenShots(float timeBetweenShots)
    {
        this.timeBetweenShots = timeBetweenShots;
    }
    /// <summary>
    /// Sets the time it takes to reload.
    /// </summary>
    /// <param name="timeToReload">Time to reload.</param>
    public void SetTimeToReload(float timeToReload)
    {
        this.timeToReload = timeToReload;
    }
    /// <summary>
    /// Sets whether the gun is currently reloading or not
    /// </summary>
    /// <param name="reloading">If set to <c>true</c> is reloading.</param>
    private void SetIsReloading(bool reloading)
    {
        this.isReloading = reloading;
    }

    /// <summary>
    /// Sets the type of the gun.
    /// </summary>
    /// <param name="gunType">Gun type.</param>
    public void SetGunType(Type gunType)
    {
        this.gunType = gunType;
    }
    /// <summary>
    /// Sets how the gun is able to shoot
    /// </summary>
    /// <param name="fireType">Fire type.</param>
    public void SetFireType(FireType fireType)
    {
        this.fireType = fireType;
    }



    /// <summary>
    /// Gets the max bullets each magazine can carry
    /// </summary>
    /// <returns>The max bullets per magazine.</returns>
    public int GetMaxBulletsPerMagazine()
    {
        return maxBulletsPerMagazine;
    }
    /// <summary>
    /// Gets the number of bullets in the current magazine.
    /// </summary>
    /// <returns>The bullets in magazine.</returns>
    public int GetBulletsInMagazine()
    {
        return bulletsInMagazine;
    }
    /// <summary>
    /// Gets the total number bullets.
    /// </summary>
    /// <returns>The total number bullets.</returns>
    public int GetTotalNumBullets()
    {
        return totalNumBullets;
    }

    /// <summary>
    /// Gets the damage per shot.
    /// </summary>
    /// <returns>The damage per shot.</returns>
    public float GetDamagePerShot()
    {
        return damagePerShot;
    }
    /// <summary>
    /// Gets the time between shots.
    /// </summary>
    /// <returns>The time between shots.</returns>
    public float GetTimeBetweenShots()
    {
        return timeBetweenShots;
    }
    /// <summary>
    /// Gets the time it takes to reload.
    /// </summary>
    /// <returns>The time to reload.</returns>
    public float GetTimeToReload()
    {
        return timeToReload;
    }
    /// <summary>
    /// Gets the shoot timer.
    /// </summary>
    /// <returns>The shoot timer.</returns>
    public float GetShootTimer()
    {
        return shootTimer;
    }
    /// <summary>
    /// Gets the reload timer.
    /// </summary>
    /// <returns>The reload timer.</returns>
    public float GetReloadTimer()
    {
        return reloadTimer;
    }

    /// <summary>
    /// Is currently reloading
    /// </summary>
    /// <returns><c>true</c>, if currently reloading, <c>false</c> otherwise.</returns>
    public bool IsReloading()
    {
        return isReloading;
    }

    /// <summary>
    /// Gets the type of the gun.
    /// </summary>
    /// <returns>The gun type.</returns>
    public Type GetGunType()
    {
        return gunType;
    }
    /// <summary>
    /// Gets how the gun is able to fire
    /// </summary>
    /// <returns>The fire type.</returns>
    public FireType GetFireType()
    {
        return fireType;
    }
}
