﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PistolObject : GunObject 
{
    /// <summary>
    /// The pistol start x.
    /// </summary>
    public static float PISTOL_START_X = 0.5f;
    /// <summary>
    /// The pistol start y.
    /// </summary>
    public static float PISTOL_START_Y = -0.25f;
    /// <summary>
    /// The pistol start z.
    /// </summary>
    public static float PISTOL_START_Z = 0.75f;



    /// <summary>
    /// How many bullets per magazine.
    /// </summary>
    private const int BULLETS_PER_MAGAZINE = 12;
    /// <summary>
    /// The damgage per shot.
    /// </summary>
    private const float DAMGAGE_PER_SHOT = 10;
    /// <summary>
    /// The time between shots.
    /// </summary>
    private const float TIME_BETWEEN_SHOTS = 100;
    /// <summary>
    /// The reload time.
    /// </summary>
    private const float RELOAD_TIME = 2000;
    /// <summary>
    /// The type of the gun.
    /// </summary>
    private const Type GUN_TYPE = Type.Pistol;
    /// <summary>
    /// The type of the fire.
    /// </summary>
    private const FireType FIRE_TYPE = FireType.Single;



    /// <summary>
    /// Start this instance.
    /// </summary>
	protected override void Start () 
    {
        base.Start();
        SetMaxBulletsPerMagazine(BULLETS_PER_MAGAZINE);
        SetBulletsInMagazine(BULLETS_PER_MAGAZINE);
        SetTotalNumBullets(48);
        SetDamagePerShot(DAMGAGE_PER_SHOT);
        SetTimeBetweenShots(TIME_BETWEEN_SHOTS);
        SetTimeToReload(RELOAD_TIME);
        SetGunType(GUN_TYPE);
        SetFireType(FIRE_TYPE);
	}
    /// <summary>
    /// Update this instance.
    /// </summary>
    protected override void Update()
    {
        base.Update();
    }
}
