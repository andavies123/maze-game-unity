﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class MainMenuButtonAction : MonoBehaviour
{

    public Camera mainCamera;
    public float smoothTime = 10f;

    private Vector3 targetPosition;
    private Vector3 velocity;

    private bool updateCamera;

    private void Start()
    {
        velocity = Vector3.zero;
        updateCamera = false;
    }

    private void Update()
    {
        if (updateCamera)
        {
            mainCamera.transform.position = Vector3.SmoothDamp(mainCamera.transform.position, targetPosition, ref velocity, smoothTime);
            if (Vector3.Distance(mainCamera.transform.position, targetPosition) < 0.1f) updateCamera = false;
        }
    }

    public void PlayButton()
    {
        SceneManager.LoadScene("Game Scene");
    }

    public void SettingsButton()
    {
        targetPosition = new Vector3(1357, 0, mainCamera.transform.position.z);
        updateCamera = true;
    }

    public void ExitButton()
    {
        #if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
        #else
            Application.Quit();
        #endif
    }

    public void SettingsBackButton() {
        targetPosition = new Vector3(0, 0, mainCamera.transform.position.z);
        updateCamera = true;
    }
}
