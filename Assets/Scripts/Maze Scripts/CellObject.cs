﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CellObject : MonoBehaviour
{
    /* Constant to hold the conversion of a plane to our cell. Planes by default are size of 10 */
    public const int DEFAULT_SIZE_OF_PLANE = 10;



    /* North points to the positive Z direction
     * South points to the negative Z direction
     * East points to the positive X direction
     * West points to the negative X direction */
    public enum Direction { North, West, South, East };
    /* X points to the x-axis
     * Z points to the z-axis */
    public enum Dimension { X, Z };
    /* Deadend means that the cell contains 3 surrounding walls
     * Walkway means that the cell contains 2 surrounding walls that are parallel
     * Corner means that the cell contains 2 surorunding walls that are perpendicular
     * Fork means that the cell contains 1 surrounding wall
     * Open means that the cell contains no walls */
    public enum Type { Deadend, Walkway, Corner, Fork, Open, Undefined }



    /* GameObject to hold the prefab for the cell
     * 0 setter methods
     * 1 getter method --> GetCellPrefab() */
    public GameObject cellPrefab;


    /* GameObjects to hold the instance of each surrounding cell
     * 1 setter method --> SetCell(GameObject, Direction)
     * 1 getter method --> GetCell(Direction) */
    private GameObject northCell;
    private GameObject westCell;
    private GameObject southCell;
    private GameObject eastCell;
    /* GameObjects to hold the instance of each surrounding wall
     * 1 setter method --> SetWall(GameObject, Direction)
     * 1 getter method --> GetWall(Direction) */
    private GameObject northWall;
    private GameObject westWall;
    private GameObject southWall;
    private GameObject eastWall;
    /* Integers to hold the X and Z position of the cells in the maze.
     * This does not hold the position in the world but only the grid position. 
     * 1 setter method --> SetCellPosition(int, Dimension)
     * 1 getter method --> GetCellPosition(Dimension) */
    private int cellPosX;
    private int cellPosZ;
    /* Floats to hold the specific size of the each cell.
     * Width and length are not used as it would be difficult to tell which is dimension is which. 
     * 1 setter method --> SetCellSize(float, Dimension)
     * 1 getter method --> GetCellSize(Dimension) */
    private float cellSizeX;
    private float cellSizeZ;
    /* Type variable to hold the type of cell
     * User can not set this variable but it is auto set everytime someone calls SetWall()
     * 0 setter methods
     * 1 getter method --> GetCellType() */
    private Type cellType;


    /* Initializes every object variable to a starting value */
    void Awake()
    {
        northWall = westWall = southWall = eastWall = null;
        northCell = westCell = southCell = eastCell = null;
        cellPosX = cellPosZ = -1;
        cellSizeX = cellSizeZ = 0f;
        cellType = Type.Undefined;
        enabled = false; // This line makes it so the update function isn't called for this script.
    }

    /* Sets the cell that is touching the current cell in a particular direction */
    public void SetCell(GameObject cell, Direction direction)
    {
        switch (direction)
        {
            case Direction.North: northCell = cell; break;
            case Direction.West: westCell = cell; break;
            case Direction.South: southCell = cell; break;
            case Direction.East: eastCell = cell; break;
        }
    }
    /* Sets the wall that is touching the current cell in a particular direction */
    public void SetWall(GameObject wall, Direction direction)
    {
        switch (direction)
        {
            case Direction.North: northWall = wall; break;
            case Direction.West: westWall = wall; break;
            case Direction.South: southWall = wall; break;
            case Direction.East: eastWall = wall; break;
        }
        AutoSetCellType();
    }
    /* Sets the position of the current cell in the maze. This is not world position */
    public void SetCellPosition(int cellPos, Dimension dimension)
    {
        switch (dimension)
        {
            case Dimension.X: cellPosX = cellPos; break;
            case Dimension.Z: cellPosZ = cellPos; break;
        }
    }
    /* Sets the size of cell */
    public void SetCellSize(float cellSize, Dimension dimension)
    {
        switch (dimension)
        {
            case Dimension.X: cellSizeX = cellSize; break;
            case Dimension.Z: cellSizeZ = cellSize; break;
        }
    }
    /* Auto sets the cellType by counting the number of surrounding walls and comparing the directions */
    private void AutoSetCellType() 
    {
        int count = 0;
        if (northWall != null) count++;
        if (westWall != null) count++;
        if (southWall != null) count++;
        if (eastWall != null) count++;

        switch(count)
        {
            case 3: cellType = Type.Deadend; break;
            case 2:
                if ((northWall != null && southWall != null) || (westWall != null && eastWall != null)) cellType = Type.Walkway;
                else cellType = Type.Corner;
                break;
            case 1: cellType = Type.Fork; break;
            case 0: cellType = Type.Open; break;
            default: cellType = Type.Undefined; break;
        }
    }



    /* Returns the cellPreab object */
    public GameObject GetCellPrefab()
    {
        return cellPrefab;
    }
    /* Returns the cell specified by a direction given */
    public GameObject GetCell(Direction direction)
    {
        switch (direction)
        {
            case Direction.North: return northCell;
            case Direction.West: return westCell;
            case Direction.South: return southCell;
            case Direction.East: return eastCell;
            default: return null;
        }
    }
    /* Returns the wall specified by a direction given */
    public GameObject GetWall(Direction direction)
    {
        switch (direction)
        {
            case Direction.North: return northWall;
            case Direction.West: return westWall;
            case Direction.South: return southWall;
            case Direction.East: return eastWall;
            default: return null;
        }
    }
    /* Returns the cell position specified by a dimension given */
    public int GetCellPosition(Dimension dimension)
    {
        switch (dimension)
        {
            case Dimension.X: return cellPosX;
            case Dimension.Z: return cellPosZ;
            default: return -1;
        }
    }
    /* Returns the cell size specified by a dimension given */
    public float GetCellSize(Dimension dimension)
    {
        switch (dimension)
        {
            case Dimension.X: return cellSizeX;
            case Dimension.Z: return cellSizeZ;
            default: return 0;
        }
    }
    /* Returns the cell type of the cell */
    public Type GetCellType() 
    {
        return cellType;
    }



    /* Static method to take in an integer and return a Direction type */
    public static Direction IntToDirection(int direction)
    {
        switch (direction)
        {
            case 0: return Direction.North;
            case 1: return Direction.West;
            case 2: return Direction.South;
            case 3: return Direction.East;
            default: return Direction.North;
        }
    }



    /* Overriden method to print out what exactly is our cell in english */
    public override string ToString() { return CellTypeToString() + CellPosToString() + CellSizeToString() + CellsToString() + WallsToString(); }
    public string CellPosToString() { return "X position: " + cellPosX + "\nZ position: " + cellPosZ + "\n\n"; }
    public string CellSizeToString() { return "Size X: " + cellSizeX + "\nSize Z: " + cellSizeZ + "\n\n"; }
    public string CellsToString() { return "North Cell: " + northCell + "\nWest Cell: " + westCell + "\nSouth Cell: " + southCell + "\nEast Cell: " + eastCell + "\n\n"; }
    public string WallsToString() { return "North Wall: " + northWall + "\nWest Wall: " + westWall + "\nSouth Wall: " + southWall + "\nEast Wall: " + eastWall + "\n\n"; }
    public string CellTypeToString() 
    {
        switch(cellType) 
        {
            case Type.Deadend: return "Deadend\n\n";
            case Type.Walkway: return "Walkway\n\n";
            case Type.Corner: return "Corner\n\n";
            case Type.Fork: return "Fork\n\n";
            case Type.Open: return "Open\n\n";
            default: return "Undefined\n\n";
        }
    }
}