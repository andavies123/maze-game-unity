﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MazeHandler : MonoBehaviour {
    // IDEA FOR LESS GAME OBJECTS IF I CAN'T GET CERTAIN OBJECTS TO DISSAPEAR
    // IF WALLS ARE NEXT TO EACH OTHER THEN I CAN MAYBE COMBINE WALLS AND MAKE IT LONGER
    // JUST AN IDEA, NEED TO LOOK UP MESHCOMBINE FIRST
    public int numCellsX;
    public int numCellsZ;

    public float sizeCellX;
    public float sizeCellZ;

    public float sizeWallWidth;
    public float sizeWallLength;
    public float sizeWallHeight;

    public int numEnemies;

    public GameObject cellPrefab;
    public GameObject wallPrefab;
    public GameObject enemyPrefab;

    private GameObject[,] cells;
    private GameObject[] enemies;

    private float timeToReset = 15;

	void Start () {
        StartCreateMaze();
        StartCoroutine("ResetMaze");
	}

    void Update() {
        if (Input.GetKeyDown(KeyCode.Q)) timeToReset--;
        else if (Input.GetKeyDown(KeyCode.E)) timeToReset++;
        if (timeToReset < 1) timeToReset = 1;
    }

    void StartCreateMaze() {
        cells = new GameObject[numCellsX, numCellsZ];
        enemies = new GameObject[numEnemies];
        CreateCells();
        InitializeCells();
        CreateWalls();
        CreateMaze();
        AddEnemies();
    }

    void CreateCells() {
        for (int x = 0; x < cells.GetLength(0); x++) {
            for (int z = 0; z < cells.GetLength(1); z++) {
                cells[x, z] = Instantiate(cellPrefab, new Vector3(x * sizeCellX + sizeCellX/2, 0, z * sizeCellZ + sizeCellZ/2), Quaternion.identity);
                cells[x, z].name = "Cell [" + x + ", " + z + "]";
                cells[x, z].transform.localScale = new Vector3(sizeCellX/CellObject.DEFAULT_SIZE_OF_PLANE, cells[x, z].transform.localScale.y, sizeCellZ/CellObject.DEFAULT_SIZE_OF_PLANE);
                cells[x, z].transform.parent = transform;
            }
        }
    }

    void InitializeCells() {
        for (int x = 0; x < cells.GetLength(0); x++) {
            for (int z = 0; z < cells.GetLength(1); z++) {
                // Sets the surrounding cells of each cell.
                if (x > 0) cells[x, z].GetComponent<CellObject>().SetCell(cells[x - 1, z], CellObject.Direction.West);
                if (x < cells.GetLength(0) - 1) cells[x, z].GetComponent<CellObject>().SetCell(cells[x + 1, z], CellObject.Direction.East);
                if (z > 0) cells[x, z].GetComponent<CellObject>().SetCell(cells[x, z - 1], CellObject.Direction.South);
                if (z < cells.GetLength(1) - 1) cells[x, z].GetComponent<CellObject>().SetCell(cells[x, z + 1], CellObject.Direction.North);

                // Sets the grid position of each cell.
                cells[x, z].GetComponent<CellObject>().SetCellPosition(x, CellObject.Dimension.X);
                cells[x, z].GetComponent<CellObject>().SetCellPosition(z, CellObject.Dimension.Z);

                // Sets the size of each cell.
                cells[x, z].GetComponent<CellObject>().SetCellSize(sizeCellX, CellObject.Dimension.X);
                cells[x, z].GetComponent<CellObject>().SetCellSize(sizeCellZ, CellObject.Dimension.Z);
            }
        }
    }

    void CreateWalls() {
        for (int x = 0; x < cells.GetLength(0); x++) {
            for (int z = 0; z <= cells.GetLength(1); z++) {
                GameObject wall = Instantiate(wallPrefab, new Vector3(x * sizeCellX + sizeCellX/2, sizeWallHeight/2, z * sizeCellZ), Quaternion.identity);
                wall.name = "Wall [" + x + ", " + z + "] horizontal";
                wall.transform.localScale = new Vector3(sizeWallLength, sizeWallHeight, sizeWallWidth);
                wall.transform.parent = transform;

                // Sets the surrounding cells for each wall and the surrounding walls for each cell.
                if(z > 0) {
                    wall.GetComponent<WallObject>().SetCell(cells[x, z - 1], WallObject.Direction.South);
                    cells[x, z - 1].GetComponent<CellObject>().SetWall(wall, CellObject.Direction.North);
                }
                if(z < cells.GetLength(1)) {
                    wall.GetComponent<WallObject>().SetCell(cells[x, z], WallObject.Direction.North);
                    cells[x, z].GetComponent<CellObject>().SetWall(wall, CellObject.Direction.South);
                }

                // Sets the size of each wall.
                wall.GetComponent<WallObject>().SetSize(sizeWallWidth, WallObject.Dimension.Width);
                wall.GetComponent<WallObject>().SetSize(sizeWallHeight, WallObject.Dimension.Height);
                wall.GetComponent<WallObject>().SetSize(sizeWallLength, WallObject.Dimension.Length);
            }
        }
        for (int x = 0; x <= cells.GetLength(0); x++) {
            for (int z = 0; z < cells.GetLength(1); z++) {
                GameObject wall = Instantiate(wallPrefab, new Vector3(x * sizeCellX, sizeWallHeight / 2, z * sizeCellZ + sizeCellZ/2), Quaternion.identity);
                wall.name = "Wall [" + x + ", " + z + "] vertical";
                wall.transform.localScale = new Vector3(sizeWallLength, sizeWallHeight, sizeWallWidth);
                wall.transform.Rotate(new Vector3(0, 90, 0));
                wall.transform.parent = transform;

                // Sets the surrounding cells for each wall and the surrounding walls for each cell.
                if(x > 0) {
                    wall.GetComponent<WallObject>().SetCell(cells[x - 1, z], WallObject.Direction.West);
                    cells[x - 1, z].GetComponent<CellObject>().SetWall(wall, CellObject.Direction.East);
                }
                if(x < cells.GetLength(0)) {
                    wall.GetComponent<WallObject>().SetCell(cells[x, z], WallObject.Direction.East);
                    cells[x, z].GetComponent<CellObject>().SetWall(wall, CellObject.Direction.West);
                }

                // Sets the size of each wall.
                wall.GetComponent<WallObject>().SetSize(sizeWallWidth, WallObject.Dimension.Width);
                wall.GetComponent<WallObject>().SetSize(sizeWallHeight, WallObject.Dimension.Height);
                wall.GetComponent<WallObject>().SetSize(sizeWallLength, WallObject.Dimension.Length);
            }
        }
    }

    void CreateMaze() {
        int[,] cellId = new int[cells.GetLength(0), cells.GetLength(1)];

        // Give each cell an id to be used in the maze creation process
        for (int x = 0; x < cellId.GetLength(0); x++) {
            for (int z = 0; z < cellId.GetLength(1); z++) {
                cellId[x, z] = x * cellId.GetLength(0) + z;
            }
        }

        // Creates the maze by searching and deleting walls.
        for (int x = 0; x < cells.GetLength(0); x++) {
            for (int z = 0; z < cells.GetLength(1); z++) {
                List<GameObject> cellChoices = new List<GameObject>();
                GameObject surroundingCell;
                int surroundingCellPosX;
                int surroundingCellPosZ;

                // Adds useable cells to our list to be randomly selected if they are not null and the cellId's don't match.
                if (cells[x, z].GetComponent<CellObject>().GetCell(CellObject.Direction.North) != null && cellId[x, z] != cellId[x, z + 1]) 
                    cellChoices.Add(cells[x, z].GetComponent<CellObject>().GetCell(CellObject.Direction.North));
                if (cells[x, z].GetComponent<CellObject>().GetCell(CellObject.Direction.West) != null && cellId[x, z] != cellId[x - 1, z])
                    cellChoices.Add(cells[x, z].GetComponent<CellObject>().GetCell(CellObject.Direction.West));
                if (cells[x, z].GetComponent<CellObject>().GetCell(CellObject.Direction.South) != null && cellId[x, z] != cellId[x, z - 1])
                    cellChoices.Add(cells[x, z].GetComponent<CellObject>().GetCell(CellObject.Direction.South));
                if (cells[x, z].GetComponent<CellObject>().GetCell(CellObject.Direction.East) != null && cellId[x, z] != cellId[x + 1, z])
                    cellChoices.Add(cells[x, z].GetComponent<CellObject>().GetCell(CellObject.Direction.East));

                // Randomly picks a cell out of the list.
                if(cellChoices.Count > 0) {
                    surroundingCell = cellChoices[Random.Range(0, cellChoices.Count)];
                    surroundingCellPosX = surroundingCell.GetComponent<CellObject>().GetCellPosition(CellObject.Dimension.X);
                    surroundingCellPosZ = surroundingCell.GetComponent<CellObject>().GetCellPosition(CellObject.Dimension.Z);

                    // Destroys the wall inbetween the two cells.
                    if (x < surroundingCellPosX) {
                        Destroy(cells[x, z].GetComponent<CellObject>().GetWall(CellObject.Direction.East));
                        cells[x, z].GetComponent<CellObject>().SetWall(null, CellObject.Direction.East);
                        cells[x + 1, z].GetComponent<CellObject>().SetWall(null, CellObject.Direction.West);
                    }
                    else if(x > surroundingCellPosX) {
                        Destroy(cells[x, z].GetComponent<CellObject>().GetWall(CellObject.Direction.West));
                        cells[x, z].GetComponent<CellObject>().SetWall(null, CellObject.Direction.West);
                        cells[x - 1, z].GetComponent<CellObject>().SetWall(null, CellObject.Direction.East);
                    }
                    else if(z < surroundingCellPosZ) {
                        Destroy(cells[x, z].GetComponent<CellObject>().GetWall(CellObject.Direction.North));
                        cells[x, z].GetComponent<CellObject>().SetWall(null, CellObject.Direction.North);
                        cells[x, z + 1].GetComponent<CellObject>().SetWall(null, CellObject.Direction.South);
                    }
                    else if(z > surroundingCellPosZ) {
                        Destroy(cells[x, z].GetComponent<CellObject>().GetWall(CellObject.Direction.South));
                        cells[x, z].GetComponent<CellObject>().SetWall(null, CellObject.Direction.South);
                        cells[x, z - 1].GetComponent<CellObject>().SetWall(null, CellObject.Direction.North);
                    }

                    // Sets the cellId for all cells with a certain id.
                    int oldCellId = cellId[surroundingCellPosX, surroundingCellPosZ];
                    for (int ix = 0; ix < cells.GetLength(0); ix++) {
                        for (int iz = 0; iz < cells.GetLength(1); iz++) {
                            if (cellId[ix, iz] == oldCellId) cellId[ix, iz] = cellId[x, z];
                        }
                    }
                }
            }
        }
    }

    private void AddEnemies() 
    {
        for (int index = 0; index < enemies.Length; index++)
        {
            enemies[index] = Instantiate(enemyPrefab, new Vector3(Random.Range(1, numCellsX)*sizeCellX+sizeCellX/2, 2, Random.Range(1, numCellsZ) * sizeCellZ+sizeCellZ/2), Quaternion.identity);
            enemies[index].transform.parent = transform;
            enemies[index].name = "Enemy " + index;
        }
    }


    IEnumerator ResetMaze() {
        while (true) {
            foreach (Transform child in transform) {
                Destroy(child.gameObject);
            }
            StartCreateMaze();

            yield return new WaitForSeconds(timeToReset);
        }
    }
}