﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallObject : MonoBehaviour {
    /* North points to the positive Z direction
     * South points to the negative Z direction
     * East points to the positive X direction
     * West points to the negative X direction */
    public enum Direction { North, West, South, East };
    /* Width is the thickness of the wall
     * Length is how long the wall is
     * Height is how tall the wall is */
    public enum Dimension { Width, Length, Height };



    /* GameObjects to hold the instance of each surrounding cell
     * At most two of these should be set at all times
     * 1 setter method --> SetCell(GameObject, Direction)
     * 1 getter method --> GetCell(Direction) */
    private GameObject northCell;
    private GameObject westCell;
    private GameObject southCell;
    private GameObject eastCell;

    /* Floats that hold the physical size of each wall in all 3 dimensions.
     * 1 setter method --> SetSize(float, Dimension)
     * 1 getter method --> GetSize(Dimension) */
    private float width;
    private float length;
    private float height;



    /* Initializes each variable to a starting point */
	void Awake () 
    {
        northCell = westCell = southCell = eastCell = null;
        width = length = height = 0;
        enabled = false; // This line makes it so the update function isn't called for this script.
	}



    /* Sets the cell that is touching the current wall in a particular direction */
    public void SetCell(GameObject cell, Direction direction) 
    {
        switch (direction) 
        {
            case Direction.North: northCell = cell; break;
            case Direction.West: westCell = cell; break;
            case Direction.South: southCell = cell; break;
            case Direction.East: eastCell = cell; break;
        }
    }
    /* Sets the size of each wall in either of the 3 dimensions */
    public void SetSize(float size, Dimension dimension) 
    {
        switch(dimension) 
        {
            case Dimension.Width: width = size; break;
            case Dimension.Length: length = size; break;
            case Dimension.Height: height = size; break;
        }
    }



    /* Returns the cell specified in a certain direction */
    public GameObject GetCell(Direction direction) 
    {
        switch (direction) 
        {
            case Direction.North: return northCell;
            case Direction.West: return westCell;
            case Direction.South: return southCell;
            case Direction.East: return eastCell;
            default: return null;
        }
    }
    /* Returns a float that corresponds to a certain size of the wall specified by the user */
    public float GetSize(Dimension dimension) 
    {
        switch(dimension) 
        {
            case Dimension.Width: return width;
            case Dimension.Height: return height;
            case Dimension.Length: return length;
            default: return 0;
        }
    }



    /* Static method that takes in an integer and converts it into a Direction Object */
    public static Direction IntToDirection(int direction) 
    {
        switch(direction) 
        {
            case 0: return Direction.North;
            case 1: return Direction.West;
            case 2: return Direction.South;
            case 3: return Direction.East;
            default: return Direction.North;
        }
    }



    /* Overriden method that prints out each wall in english terms */
    public override string ToString() { return SizeToString() + CellsToString(); }
    public string SizeToString() { return "Width: " + width + "\nLength: " + length + "\nHeight: " + height + "\n\n"; }
    public string CellsToString() { return "North Cell: " + northCell + "\nWest Cell: " + westCell + "\nSouth Cell: " + southCell + "\nEast Cell: " + eastCell + "\n\n"; }
}