﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveableObject : MonoBehaviour {

	protected virtual void Start () {
        Rbody = GetComponent<Rigidbody>();
	}

    protected virtual void Update() {

    }

    public Rigidbody Rbody { get; private set; }
    public float Health { get; set; }
    public float Speed { get; set; }
    public bool IsAlive { get; set; }
    public bool IsGrounded { get; set; }
}
