﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerActionController : MonoBehaviour {
    private PlayerObject player;
    private Vector3 moveDirection;

    public GameButtonManager gameButtonManager;



	void Start () {
        player = PlayerObject.player;
        moveDirection = Vector3.zero;
        enabled = false; // Update function does not get called
	}



    public void UpdateAction() {
        if(player.GetCurrentWeapon().GetFireType() == GunObject.FireType.Auto) { // If the gun is on auto
            if (Input.GetMouseButton(0)) { // Player can hold down the fire button
                if (player.Shoot()) gameButtonManager.Hit(); // Show a hitmarker
            }
        }
        else if(player.GetCurrentWeapon().GetFireType() == GunObject.FireType.Single) { // If the gun is on single fire mode
            if (Input.GetMouseButtonDown(0)) { // Player can only shoot once per click
                if (player.Shoot()) gameButtonManager.Hit(); // Show a hitmarker
            }
        }

        if (Input.GetKeyDown(KeyCode.Space)) player.Jump();
        if (Input.GetKeyDown(KeyCode.LeftShift)) player.Run();
        if (Input.GetKeyUp(KeyCode.LeftShift)) player.Walk();

        if(Input.GetKeyDown(KeyCode.R)) player.GetCurrentWeapon().Reload(); // Reload the weapon
        UpdateDirection();
    }
    public void UpdateDirection() {
        moveDirection = new Vector3(Input.GetAxis("Horizontal"), moveDirection.y, Input.GetAxis("Vertical")); // Sets the direction to input from the keyboard
        moveDirection = transform.TransformDirection(moveDirection); // Changes the direction to world space
        moveDirection = moveDirection.normalized; // Sets the length of vector down to 1
    }

    public void Move() {
        player.GetPlayerRigidBody().MovePosition(player.GetPlayerRigidBody().position + moveDirection * player.GetCurrentSpeed() * Time.fixedDeltaTime);
    }
}