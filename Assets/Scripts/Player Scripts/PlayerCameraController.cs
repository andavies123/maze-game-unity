﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCameraController : MonoBehaviour 
{
    /* Constant float that holds the upper vertical angle the player can view */
    private const float MAX_VERTICAL = 60;
    /* Constant float that holds the lower vertical angle the player can view */
    private const float MIN_VERTICAL = -60;



    /* Float to hold the look sensitivity when the player looks up and down 
     * 1 setter --> SetVerticalSensitivity(float)
     * 1 getter --> GetVerticalSensitivity() */
    private float verticalLookSensitivity;
    /* Float to hold the look sensitivity when the player looks left and right 
     * 1 setter --> SetHorizontalSensitivity(float)
     * 1 getter --> GetHorizontalSensitivity() */
    private float horizontalLookSensitivity;
    /* Float to hold the camera rotation on the vertical Rotation
     * Global because it needs to work through multiple iterations 
     * 0 setters
     * 0 getters */
    private float verticalRotation;
    /* Float to hold the camera rotation on the horizontal axis
     * This is only global because verticalRotation is and I like symmetry 
     * 0 setters
     * 0 getters */
    private float horizontalRotation;



    /* Initializes all private variables */
    void Start () 
    {
        SetVerticalLookSensitivity(5);
        SetHorizontalLookSensitivity(5);
        enabled = false; // Update function won't be called
	}



    /* Sets the vertical look sensitivity */
    public void SetVerticalLookSensitivity(float verticalLookSensitivity)
    {
        this.verticalLookSensitivity = verticalLookSensitivity;
    }
    /* Sets the horizontal look sensitivity */
    public void SetHorizontalLookSensitivity(float horizontalLookSensitivity)
    {
        this.horizontalLookSensitivity = horizontalLookSensitivity;
    }



    /* Returns a float that corresponds to the vertical look sensitivity */
    public float GetVerticalLookSensitivity()
    {
        return verticalLookSensitivity;
    }
    /* Returns a float that corresponds to the horizontal look sensitivity */
	public float GetHorizontalLookSensitivity()
    {
        return horizontalLookSensitivity;
    }



    /* Update function that gets called by the Player object */
    public void UpdateLook() 
    {
        UpdateRotation();
    }
    /* Gets input from the mouse to rotate the camera */
    private void UpdateRotation()
    {
        horizontalRotation = transform.parent.transform.localEulerAngles.y + Input.GetAxis("Mouse X") * horizontalLookSensitivity;
        verticalRotation += Input.GetAxis("Mouse Y") * verticalLookSensitivity;
        verticalRotation = Mathf.Clamp(verticalRotation, MIN_VERTICAL, MAX_VERTICAL);

        transform.parent.transform.localEulerAngles = new Vector3(0, horizontalRotation, 0);
        transform.localEulerAngles = new Vector3(-verticalRotation, 0, 0);
    }
}
