﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerObject : MonoBehaviour 
{
    public static PlayerObject player;



    /* Primary corresponds to the primary weapon
     * Secondary corresponds to the secondary weapon 
     * Empty corresponds to being empty handed */
    public enum GunType { Primary, Secondary, Empty }



    private const float WALKING_SPEED = 5f;
    private const float RUNNING_SPEED = 8f;
    private const float JUMP_FORCE = 300f;
    private const float MAX_HEALTH = 100f;



    private GameObject playerCamera;
    private Rigidbody playerRigidBody;

    private PlayerActionController actionController;
    private PlayerCameraController cameraController;

    private GunObject primaryWeapon;
    private GunObject secondaryWeapon;
    private GunObject currentWeapon;

    private GunType currentGunType;

    private float health;
    private float currentSpeed;

    private bool alive;
    private bool grounded;





	void Awake () {
        player = this;
        SetPlayerCamera(GameObject.Find("Camera"));
        SetPlayerRigidBody(GetComponent<Rigidbody>());

        SetActionController(GetComponent<PlayerActionController>());
        SetCameraController(GetComponentInChildren<PlayerCameraController>()); // Camera is held has a child of the current object

        SetPrimaryWeapon(null);
        SetSecondaryWeapon(GameObject.Find("Gun Pistol").GetComponent<GunObject>());

        SetCurrentGunType(GunType.Secondary);

        SetHealth(MAX_HEALTH);
        SetCurrentSpeed(WALKING_SPEED);

        SetAlive(true);
	}
    void Update() {
        actionController.UpdateAction();
        cameraController.UpdateLook();
    }
    void FixedUpdate() { actionController.Move(); }
    //void OnCollisionEnter(Collision collision) { if (collision.gameObject.name.Contains("Cell")) SetGrounded(true); } // If the player is touching the ground





    public void Run() { SetCurrentSpeed(RUNNING_SPEED); }
    public void Walk() { SetCurrentSpeed(WALKING_SPEED); }
    public void Jump() {
        if (IsGrounded()) { // Checks if the player is on the ground
            GetPlayerRigidBody().AddForce(transform.up * JUMP_FORCE); // Adds a jump force
            SetGrounded(false); // Sets it so the player is not on the ground anymore
        }
    }
    public bool Shoot() {
        GameObject shotGameObject = player.GetCurrentWeapon().Shoot(playerCamera.transform); // Shot coming from the camera's transform
        if (shotGameObject != null && shotGameObject.name.Contains("Enemy")) { // If the player shot an enemy
            shotGameObject.GetComponent<EnemyController>().Hurt(player.GetCurrentWeapon().GetDamagePerShot()); // Hurt the enemy with gun damage
            return true;
        }
        return false;
    }
    public void Heal(float health) { SetHealth(GetHealth() + health); }
    public void Hurt(float health) { SetHealth(GetHealth() - health); }




    public void SetPlayerCamera(GameObject playerCamera) { this.playerCamera = playerCamera; }
    public void SetPlayerRigidBody(Rigidbody playerRigidBody) { this.playerRigidBody = playerRigidBody; }

    public void SetActionController(PlayerActionController actionController) { this.actionController = actionController; }
    public void SetCameraController(PlayerCameraController cameraController) { this.cameraController = cameraController; }

    public void SetPrimaryWeapon(GunObject primaryWeapon) { this.primaryWeapon = primaryWeapon; }
    public void SetSecondaryWeapon(GunObject secondaryWeapon) { this.secondaryWeapon = secondaryWeapon; }
    public void SetCurrentWeapon(GunObject currentWeapon) { this.currentWeapon = currentWeapon; }

    public void SetCurrentGunType(GunType currentGunType) {
        this.currentGunType = currentGunType;
        switch(currentGunType) { // Also sets the current weapon at the same time
            case GunType.Primary:
                SetCurrentWeapon(GetPrimaryWeapon());
                break;
            case GunType.Secondary:
                SetCurrentWeapon(GetSecondaryWeapon());
                break;
            case GunType.Empty:
                SetCurrentWeapon(null);
                break;
        }
    }

    public void SetHealth(float health) { 
        this.health = health;
        if (GetHealth() > MAX_HEALTH) this.health = MAX_HEALTH; // Health can't go over max heatlh
        else if (GetHealth() <= 0) SetAlive(false); // If health is 0 or under then the player is dead
    }
    public void SetCurrentSpeed(float currentSpeed) { this.currentSpeed = currentSpeed; }

    public void SetAlive(bool alive) { this.alive = alive; }
    public void SetGrounded(bool grounded) { this.grounded = grounded; }





    public GameObject GetPlayerCamera() { return playerCamera; }
    public Rigidbody GetPlayerRigidBody() { return playerRigidBody; }

    public PlayerActionController GetActionController() { return actionController; }
    public PlayerCameraController GetCameraController() { return cameraController; }

    public GunObject GetPrimaryWeapon() { return primaryWeapon; }
    public GunObject GetSecondaryWeapon() { return secondaryWeapon; }
    public GunObject GetCurrentWeapon() { return currentWeapon; }

    public GunType GetCurrentGunType() { return currentGunType; }

    public float GetHealth() { return health; }
    public float GetCurrentSpeed() { return currentSpeed; }

    public bool IsAlive() { return alive; }
    public bool IsGrounded() { return grounded; }
}